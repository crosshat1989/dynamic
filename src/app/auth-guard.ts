import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable()

export class AuthGuardService implements CanActivate {
  constructor(public router: Router) {}
  canActivate(): boolean {
    const check = localStorage.getItem('login');
        // use jwt token in real scenerio here
    if (check) {
        return true;
    } else {
        this.router.navigate(['login']);
        return false;
    }
}
}