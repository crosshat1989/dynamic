import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  target: string = '';
  constructor(private router: Router) { }

  login() {
    if (this.username === "jawaria" && this.password === "arshad") {
      localStorage.setItem("login", "true");
      this.router.navigate(['dashboard']);
    }
  }

  ngOnInit() {
  }

}
