import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChartModule } from 'angular-highcharts';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { OnsenModule } from 'ngx-onsenui';
import {MatSelectModule} from '@angular/material/select';

@NgModule({
  imports: [
    CommonModule,
    ChartModule,
    FlexLayoutModule,
    OnsenModule,
    MatSelectModule
  ],
  declarations: [DashboardComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
]
})
export class DashboardModule { }
