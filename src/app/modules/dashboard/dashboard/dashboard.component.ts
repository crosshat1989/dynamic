import { Component, OnInit, AfterViewInit, DoCheck } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { Options, LineChartSeriesOptions } from 'highcharts';
import { DynamicService } from '../../../core/services/core.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [DynamicService]
})
export class DashboardComponent implements OnInit {
  chart1 = new Chart;
  chart2 = new Chart;
  chart3 = new Chart;
  options: Options;
  data: any;

  // Aggregated data declarations
  chartIncome = [];
  aYear = [];
  aIndexedIncome = [];
  aForceClosureValue = [];
  selectedModifier: string = '1';
  modifiers = [
    { value: 1, label: 'Total Income' },
    { value: 2, label: 'Indexed Total Income' },
    { value: 3, label: 'Force Closure Value' }
  ];
  checkChange: any;


  // property value declarations
  chartProperty = [];
  propertyYear = [];
  loadChart1 = false;
  loadChart2 = false;
  loadChart3 = false;

  // LTFV value declarations

  ltfvYear = [];
  ltfvValue = [];

  constructor(private dynamicService: DynamicService, private router: Router) { }

  // add point to chart serie
  add() {
    this.chart1.addPoint(Math.floor(Math.random() * 10));
  }
  ngAfterViewInit() {
  }
  displayChart1(value?: any, label?: any) {
    if (value === undefined) {
      value = this.chartIncome;
      label = 'Total Income'
    } else {
      switch (value[0]) {
        case 1: {
          value = this.chartIncome;
          break;
        }
        case 2: {
          value = this.aIndexedIncome;
          break;
        }
        case 3: {
          value = this.aForceClosureValue;
          break;
        }
      }

    }
    this.chart1 = new Chart({
      chart: {
        type: 'column'
      },
      xAxis: {
        categories: this.aYear
      },
      title: {
        text: 'Aggregated Result'
      },
      credits: {
        enabled: false
      },
      series: [
        {
          name: label,
          data: value
        }
      ]
    });
  }

  displayChart2() {
    this.chart2 = new Chart({
      chart: {
        type: 'column'
      },
      xAxis: {
        categories: this.propertyYear
      },
      colors:['indigo'],
      title: {
        text: 'Original Property Value'
      },
      credits: {
        enabled: false
      },
      series: [
        {
          name: 'Value in USD',
          data: this.chartProperty
        }
      ]
    });
  }

  displayChart3() {
    this.chart3 = new Chart({
      chart: {
        type: 'column'
      },
      colors:['purple'],
      xAxis: {
        categories: this.ltfvYear
      },
      title: {
        text: 'Indexed LTFV'
      },
      credits: {
        enabled: false
      },
      series: [
        {
          name: 'Value',
          data: this.ltfvValue
        }
      ]
    });
  }

  // Get aggregated results for the panel
  getResults() {
    this.dynamicService.getResults().subscribe((data: any) => {
      this.data = data;
    });
  }

  // Get aggregated results for the chart
  getResultsByYear() {
    this.dynamicService.getResultsByYear().subscribe((data: any) => {
      for (let i = 0; i <= data.length; i++) {
        if (data[i] !== undefined) {
          this.chartIncome.push(data[i]['waTotalIncome'])
          this.aYear.push(data[i]['loanOriginationYear'])
          this.aIndexedIncome.push(data[i]['waIndexedTotalIncome'])
          this.aForceClosureValue.push(data[i]['waOriginalForeclosureValue'])
        }
      }
        ; this.displayChart1();
      this.loadChart1 = true;
    });
  }

  // Get propert value for the chart
  getPropertyValue() {
    this.dynamicService.getPropertyValue().subscribe((data: any) => {
      for (let i = 0; i <= data.length; i++) {
        if (data[i] !== undefined) {
          this.chartProperty.push(data[i]['averageOriginalPropertyValue']);
          this.propertyYear.push(data[i]['loanOriginationYear']);
        }
      }
      this.displayChart2();
      this.loadChart2 = true;
    });
  }

  getLtfv() {
    this.dynamicService.getLtfv().subscribe((data: any) => {
      for (let i = 0; i <= data.length; i++) {
        if (data[i] !== undefined) {
          this.ltfvValue.push(data[i]['averageIndexedLTFV']);
          this.ltfvYear.push(data[i]['loanOriginationYear']);
        }
      }
      this.displayChart3();
      this.loadChart3 = true;
    });
  }

  logout() {
    localStorage.removeItem("login");
    this.router.navigate(['login']);
}

  ngOnInit() {
    this.getResults();
    this.getResultsByYear();
    this.getPropertyValue();
    this.getLtfv();
  }

}
