import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
// import 'rxjs/add/operator/mergeMap';
// import 'rxjs/add/operator/switchMap';
// import 'rxjs/add/operator/catch';
@Injectable()

export class DynamicService {
  // For real project urls will be entered in the environment variables... 
  private url1 = "http://localhost:57785/api/aggregatedResult"
  private url2 = "http://localhost:57785/api/aggregatedResultByYear"
  private url3 = "http://localhost:57785/api/averageOriginalPropertyValueByYear"
  private url4 = "http://localhost:57785/api/averageindexedLTFVByYear"

  public newData: any;
  public filters = [];
  public processedFilters = [];
  masterParam: string;
  inexFilter = [];
  constructor(private http: HttpClient) { }

  getResults(): Observable<any> {
    const result = this.http.get(this.url1);
    return result.pipe(map((data: any) => {
      if (data === null) {
        alert('No Data!');
      }
      this.newData = data;
      return this.newData;
    }))
      .pipe(catchError((err: HttpErrorResponse) => {
        console.log(err.status);
        alert('Internal Server Error or No data available!!');
        location.reload();
        return Observable.throw(err.statusText);
      }));
  }

  getResultsByYear(): Observable<any> {
    const result = this.http.get(this.url2);
    return result.pipe(map((data: any) => {
      if (data === null) {
        alert('No Data!');
      }
      this.newData = data;
      return this.newData;
    }))
      .pipe(catchError((err: HttpErrorResponse) => {
        console.log(err.status);
        alert('Internal Server Error or No data available!!');
        location.reload();
        return Observable.throw(err.statusText);
      }));
  }

  getPropertyValue(): Observable<any> {
    const result = this.http.get(this.url3);
    return result.pipe(map((data: any) => {
      if (data === null) {
        alert('No Data!');
      }
      this.newData = data;
      return this.newData;
    }))
      .pipe(catchError((err: HttpErrorResponse) => {
        console.log(err.status);
        alert('Internal Server Error or No data available!!');
        location.reload();
        return Observable.throw(err.statusText);
      }));
  }

  getLtfv(): Observable<any> {
    const result = this.http.get(this.url4);
    return result.pipe(map((data: any) => {
      if (data === null) {
        alert('No Data!');
      }
      this.newData = data;
      return this.newData;
    }))
      .pipe(catchError((err: HttpErrorResponse) => {
        console.log(err.status);
        alert('Internal Server Error or No data available!!');
        location.reload();
        return Observable.throw(err.statusText);
      }));
  }

}